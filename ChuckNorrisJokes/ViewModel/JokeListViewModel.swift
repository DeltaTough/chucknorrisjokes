import Combine
import SwiftUI

class JokeListViewModel: ObservableObject {
    @Published private(set) var isBusy = false
    @Published var jokes: [Joke] = []
    @Published var errorResult: ErrorResult?

    private let jokeCountPublisher: JokeCountPublisher
    private let jokePublisher: JokePublisher
    private var cancellable: AnyCancellable?

    init(jokeCountPublisher: JokeCountPublisher = ApiClient(),
         jokePublisher: JokePublisher = ApiClient()) {
        self.jokeCountPublisher = jokeCountPublisher
        self.jokePublisher = jokePublisher
    }

    func fetchJokes() {
        isBusy = true
        errorResult = nil
        cancellable = jokeCountPublisher.getJokeCount()
            .flatMap {
                self.jokePublisher.getJokes(count: $0)
                    .receive(on: DispatchQueue.main)
                    .map { self.jokes.append(contentsOf: $0) }
                    .eraseToAnyPublisher()
            }.eraseToAnyPublisher()
            .sink(receiveCompletion: { [weak self] completion in
                self?.isBusy = false
                if case .failure = completion {
                    self?.setErrorResult()
                }
            }, receiveValue: { _ in })
    }

    private func setErrorResult() {
        errorResult = ErrorResult(title: "",
                                  message: "There was an error fetching jokes")
    }
}


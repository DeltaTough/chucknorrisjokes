import SwiftUI

struct JokeListView: View {
    @StateObject var viewModel = JokeListViewModel()
    var body: some View {
        VStack {
            NavigationView {
                List {
                    ForEach(viewModel.jokes, id: \.self) { joke in
                        Text(joke.joke.specialCharactersDecoded()).padding()

                    }
                }.overlay(ProgressView()
                            .opacity(viewModel.isBusy ? 1 : 0))
                .navigationBarTitle("Jokes")
            }
            .navigationViewStyle(StackNavigationViewStyle())
        }.onAppear {
            viewModel.fetchJokes()
        }
        .alert(item: $viewModel.errorResult) { error in
            Alert(
                title: Text(error.title),
                message: Text(error.message),
                dismissButton: .default(Text("OK")) {
                    viewModel.errorResult = nil
                })
        }
    }
}

struct JokeListView_Previews: PreviewProvider {
    static var previews: some View {
        Preview()
    }

    private struct Preview: View {
        private let viewModel = JokeListViewModel()

        init() {
            viewModel.jokes.append(createJoke())
        }

        var body: some View {
            JokeListView(viewModel: viewModel)
        }
    }
}

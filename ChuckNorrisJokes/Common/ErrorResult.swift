struct ErrorResult: Identifiable {
    var id: String { message }
    let title: String
    let message: String
}

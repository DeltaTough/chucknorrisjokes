import UIKit

extension String {
    func specialCharactersDecoded() -> String {

        guard (self != "") else { return self }

        var newString = self

        let entities = [
            "&quot;"    : "\"",
            "&amp;"     : "&",
            "&apos;"    : "'",
            "&lt;"      : "<",
            "&gt;"      : ">",
        ]

        while entities.keys.filter({ newString.contains($0) }).count != 0 {
            for (index, value) in entities {
                newString = newString.replacingOccurrences(of: index, with: value)
            }
        }
        return newString
    }
}

import Combine
import UIKit

enum HttpMethod: String {
    case get = "GET"
}

final class ApiClient {
    var urlSession = URLSession(configuration: .default)

    static func createRequest(urlString: String, method: HttpMethod = .get, data: Data? = nil) -> URLRequest? {
        guard let url = URL(string: urlString) else {
            return nil
        }
        var req = URLRequest(url: url)
        req.httpMethod = method.rawValue
        if method != .get, data != nil {
            req.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        }
        req.setValue("application/json", forHTTPHeaderField: "Accept")
        req.httpBody = data

        return req
    }

    func sendPublisher<T>(request: URLRequest) -> AnyPublisher<T, ApiError> where T: Decodable {
        return urlSession.dataTaskPublisher(for: request)
            .catch { ApiClient.catchErrorPublished(error: $0) }
            .flatMap { data, response -> AnyPublisher<T, ApiError> in
                guard let response = response as? HTTPURLResponse else { return Fail(error: .generic).eraseToAnyPublisher() }

                if T.self == Bool.self, let isSuccess = (200 ..< 300 ~= response.statusCode) as? T {
                    return Just(isSuccess).setFailureType(to: ApiError.self).eraseToAnyPublisher()
                }

                if 200 ..< 300 ~= response.statusCode {
                    let deserialiseResult: Result<T, ApiError> = ApiClient.deserialise(data: data)
                    switch deserialiseResult {
                    case let .success(res):
                        return Just(res).setFailureType(to: ApiError.self).eraseToAnyPublisher()
                    case let .failure(error):
                        return Fail(error: error).eraseToAnyPublisher()
                    }
                }

                return Fail(error: .generic).eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }

    private static func deserialise<T>(data: Data) -> Result<T, ApiError> where T: Decodable {
        let decoder = JSONDecoder()

        do {
            return try .success(decoder.decode(T.self, from: data))
        } catch {
            var message = "Decode of \(String(describing: T.self)) has failed."
            if let stringData = String(data: data, encoding: .utf8) {
                message.append(" data: \(stringData)")
            }
            return .failure(.deserialisationFailed)
        }
    }

    private static func catchErrorPublished<T>(error: URLError) -> AnyPublisher<T, ApiError> {
        if error.code == .notConnectedToInternet {
            return Fail(error: .noConnection).eraseToAnyPublisher()
        }
        return Fail(error: .generic).eraseToAnyPublisher()
    }
}


enum ApiError: Error {
    case noConnection
    case deserialisationFailed
    case generic

    func message() -> String {
        switch self {
        case .noConnection:
            return "Please check your connection"
        case .deserialisationFailed:
            return "DeserializationFailed"
        default:
            return "There was an error"
        }
    }
}

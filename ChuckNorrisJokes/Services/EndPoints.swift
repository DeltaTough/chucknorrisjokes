enum EndPoints {
    static var baseURLString = "https://api.icndb.com/jokes/"

    case jokeCount
    case jokeList(count: Int)

    func url() -> String {
        switch self {
        case .jokeCount:
            return EndPoints.baseURLString.appending("count")
        case let .jokeList(count: count):
            return EndPoints.baseURLString.appending("random/\(count)?exclude=explicit")
        }
    }
}


import Combine

struct JokeApi: Decodable {
    let type: String
    let value: [Joke]
}

struct Joke: Decodable, Identifiable, Hashable {
    let id: Int
    let joke: String
    let categories: [String]
}

protocol JokePublisher {
    func getJokes(count: Int) -> AnyPublisher<[Joke], ApiError>
}

extension ApiClient: JokePublisher {
    func getJokes(count: Int) -> AnyPublisher<[Joke], ApiError> {
        guard let request = ApiClient.createRequest(urlString: EndPoints.jokeList(count: count).url()) else {
            return Fail(error: .generic).eraseToAnyPublisher()
        }

        return sendPublisher(request: request)
            .map { (jokeAPi: JokeApi) in
                return jokeAPi.value
            }
            .eraseToAnyPublisher()
    }
}

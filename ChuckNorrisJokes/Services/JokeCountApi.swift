import Combine

struct JokeCountApi: Decodable {
    let type: String
    let value: Int
}

protocol JokeCountPublisher {
    func getJokeCount() -> AnyPublisher<Int, ApiError>
}

extension ApiClient: JokeCountPublisher {
    func getJokeCount() -> AnyPublisher<Int, ApiError> {
        guard let request = ApiClient.createRequest(urlString: EndPoints.jokeCount.url()) else {
            return Fail(error: .generic).eraseToAnyPublisher()
        }

        return sendPublisher(request: request)
            .map { (jokeCount: JokeCountApi) in
                return jokeCount.value
            }
            .eraseToAnyPublisher()
    }
}

@testable import ChuckNorrisJokes
import Combine
import XCTest

class ApiClientTests: XCTestCase {
    private var apiClient: ApiClient!
    private var cancellable: AnyCancellable?

    let mockURLSessionConfig: URLSessionConfiguration = {
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [MockURLProtocol.self]
        return config
    }()

    struct SomeIntResponse: Decodable {
        let some_int: Int
    }

    override func setUp() {
        MockURLProtocol.requestHandler = nil
        apiClient = ApiClient()
        apiClient.urlSession = URLSession(configuration: mockURLSessionConfig)
        cancellable = nil
    }

    override func tearDown() {
        MockURLProtocol.requestHandler = nil
        cancellable = nil
    }

    func testSendSimpleBoolResponseFail() throws {
        MockURLProtocol.requestHandler = { request in
            let response = HTTPURLResponse(url: request.url!, statusCode: 300, httpVersion: nil, headerFields: nil)!
            return (response, Data())
        }

        let request = ApiClient.createRequest(urlString: "dummy")!
        let result: AnyPublisher<Bool, ApiError> = apiClient.sendPublisher(request: request)
        cancellable = result
            .sink(receiveCompletion: { _ in },
                  receiveValue: {
                XCTAssertFalse($0)
            })
    }

    func testSendCatchNoConnectionError() throws {
        MockURLProtocol.requestHandler = { _ in
            throw NSError(domain: NSURLErrorDomain, code: NSURLErrorNotConnectedToInternet)
        }

        let request = ApiClient.createRequest(urlString: "dummy")!
        let result: AnyPublisher<Bool, ApiError> = apiClient.sendPublisher(request: request)

        cancellable = result
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    XCTFail("should have failed")
                case .failure(let error):
                    XCTAssertEqual(error, .noConnection)
                }
            },receiveValue: { _ in })
    }

    func testDeserialiseFailOnInvalidJSON() throws {
        MockURLProtocol.requestHandler = { request in
            let response = HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: nil, headerFields: nil)!
            return (response, Data())
        }

        let request = ApiClient.createRequest(urlString: "dummy")!
        let result: AnyPublisher<SomeIntResponse, ApiError> = apiClient.sendPublisher(request: request)
        cancellable = result
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    XCTFail("should have failed")
                case .failure(let error):
                    XCTAssertEqual(error, .deserialisationFailed)
                }
            },receiveValue: { _ in })
    }
}

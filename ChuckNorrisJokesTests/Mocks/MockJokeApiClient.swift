@testable import ChuckNorrisJokes
import Combine

class MockJokeApiClient: JokePublisher {
    var result: AnyPublisher<[Joke], ApiError> = Result.success([createJoke()]).publisher.eraseToAnyPublisher()
    var error: ApiError?
    var getJokeCallCount = 0
    
    func getJokes(count: Int) -> AnyPublisher<[Joke], ApiError> {
        getJokeCallCount += 1
        if let error = error {
            return Fail(error: error).eraseToAnyPublisher()
        } else {
            return result
        }
    }
}

@testable import ChuckNorrisJokes

func createCount(type: String = "success",
                 value: Int = 574) -> JokeCountApi {
    JokeCountApi(type: type, value: value)
}

func createJoke(id: Int = 49,
                joke: String = "As the &amp;quot;Chuck Norris Network&amp;quot; to update Americans.",
                categories: [String] = ["nerdy"]) -> Joke {
    Joke(id: id, joke: joke, categories: categories)
}


@testable import ChuckNorrisJokes
import Combine

class MockJokeCountApiClient: JokeCountPublisher {
    var result: AnyPublisher<Int, ApiError> = Result.success(createCount().value).publisher.eraseToAnyPublisher()
    var error: ApiError?
    var getJokeCountCallCount = 0

    func getJokeCount() -> AnyPublisher<Int, ApiError> {
        getJokeCountCallCount += 1
        if let error = error {
            return Fail(error: error).eraseToAnyPublisher()
        } else {
            return result
        }
    }
}

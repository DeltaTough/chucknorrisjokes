@testable import ChuckNorrisJokes
import XCTest

class StringExtensionsTests: XCTestCase {
    func testSpecialCharactersDecoded() throws {
        let result = createJoke().joke.specialCharactersDecoded()
        XCTAssertEqual(result, "As the \"Chuck Norris Network\" to update Americans.")
    }
}

@testable import ChuckNorrisJokes
import Combine
import XCTest

class JokeListViewModelTests: XCTestCase {
    private var mockJokeCountApiClient: MockJokeCountApiClient!
    private var mockJokeApiClient: MockJokeApiClient!
    private var viewModel: JokeListViewModel!
    private var cancellable: AnyCancellable?

    override func setUp() {
        mockJokeCountApiClient = MockJokeCountApiClient()
        mockJokeApiClient = MockJokeApiClient()
        viewModel = JokeListViewModel(jokeCountPublisher: mockJokeCountApiClient, jokePublisher: mockJokeApiClient)
    }

    override func tearDown() {
        cancellable = nil
    }

    func testFetchJokesSucceeds() throws {
        var isBusyCalls = [Bool]()
        cancellable = viewModel.$isBusy
            .sink(receiveValue: {
                isBusyCalls.append($0)
            })
        viewModel.fetchJokes()
        wait(for: 1)
        XCTAssertEqual(self.mockJokeCountApiClient.getJokeCountCallCount, 1)
        XCTAssertEqual(self.mockJokeApiClient.getJokeCallCount, 1)
        XCTAssertEqual(isBusyCalls.count, 3)
        XCTAssertFalse(isBusyCalls[0])
        XCTAssertTrue(isBusyCalls[1])
        XCTAssertFalse(isBusyCalls[2])
        XCTAssertTrue(viewModel.errorResult == nil)
    }

    func testFetchJokesFailedWithAlert() throws {
        var errorMessage: ErrorResult? = nil
        mockJokeCountApiClient.error = .generic
        cancellable = viewModel.$errorResult
            .sink(receiveValue: {
                errorMessage = $0
            })
        viewModel.fetchJokes()
        wait(for: 1)
        XCTAssertEqual(self.mockJokeCountApiClient.getJokeCountCallCount, 1)
        XCTAssertEqual(self.mockJokeApiClient.getJokeCallCount, 0)
        XCTAssertTrue(errorMessage?.message == "There was an error fetching jokes")
    }
}

